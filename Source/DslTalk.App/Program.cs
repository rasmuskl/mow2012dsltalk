﻿using System;
using System.Collections.Generic;
using System.Linq;
using DslTalk.App.Config;
using DslTalk.Configuration;
using DslTalk.Generator;
using DslTalk.Models;
using DslTalk.Output;

namespace DslTalk.App
{
    public class Program
    {
        #region People

        private static readonly Person[] _people = new[]
            {
                new Person
                {
                    Name = "Rasmus", 
                    Age = 30, 
                    Address = "Somewhere in Copenhagen",
                    TwitterHandle = "@rasmuskl"
                },
                new Person
                {
                    Name = "Alice", 
                    Age = 35, 
                    Address = "USA",
                    Email = "alice@example.com",
                },
                new Person
                {
                    Name = "Bob", 
                    Age = 27, 
                    TwitterHandle = "@therealbob"
                },
            };

        #endregion

        public static void Main()
        {
            UsingModelDirectly();
            //Fluently();
        }

        private static void UsingModelDirectly()
        {
            Console.WriteLine("--- Using Model directly:" + Environment.NewLine);

            var model = new ClassOutputModel(typeof(Person));

            PropertyOutputModel nameModel = model.GetPropertyModel("Name");
            nameModel.Color = OutputColor.Green;

            PropertyOutputModel ageModel = model.GetPropertyModel("Age");
            ageModel.Reversed = true;

            foreach (Person person in _people)
            {
                RenderToConsole(model, person);
            }
        }

        private static void Fluently()
        {
            Console.WriteLine("--- Fluently:" + Environment.NewLine);

            Format.Configure(c =>
                {
                    c.For<Person>()
                        .Write(x => x.Name, x => x.Rainbow().AllCaps())
                        .Write(x => x.Age)
                        .Write(x => x.TwitterHandle, x => x.Green().OmitIf(string.IsNullOrWhiteSpace))
                        .Write(x => x.Email, x => x.OmitIf(string.IsNullOrWhiteSpace));
                });

            foreach (Person person in _people)
            {
                RenderToConsole(Format.GetStringFormatter<Person>(), person);
            }
        }

        public static void FluentlyAlternativeConfig()
        {
            Format.AddRegistry<PersonFormat>();

            // or

            Format.ScanAssembliesForRegistries();
        }

        private static void GeneratedGrammar(IGeneratorGrammar grammar)
        {
            Format.Configure(c =>
                {
                    c.For<Person>()
                        .Write(x => x.Name, x => x.Rainbow().Green().Red());
                });

            var lang = new GeneratorGrammarLang(grammar);

            
        }

        private static void Conventions()
        {
            Format.AddConvention(c =>
                {
                    c.Properties.OfType<int>().Write(x => x.Rainbow());
                });
        }

        #region Helpers

        private static void RenderToConsole(ClassOutputModel classOutputMap, Person person)
        {
            IEnumerable<IOutput> outputs = classOutputMap.BuildOutput(person);

            var renderer = new ConsoleOutputRenderer();
            renderer.Render(outputs);

            Console.WriteLine(string.Empty);
        }

        #endregion
    }
}
