using DslTalk.Configuration;

namespace DslTalk.App.Config
{
    public class PersonFormat : FormatRegistry<Person>
    {
        public PersonFormat()
        {
            Write(x => x.Name).Green();

            Write(x => x.Age).OmitIf(x => x > 50);

            Write(x => x.Address).Reversed();

            Write(x => x.TwitterHandle);
            Write(x => x.Email);
        }
    }
}