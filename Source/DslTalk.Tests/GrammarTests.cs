﻿using System.Collections.Generic;
using DslTalk.Configuration;
using DslTalk.Models;
using DslTalk.Output;
using NUnit.Framework;
using System.Linq;

namespace DslTalk.Tests
{
    [TestFixture]
    public class GrammarTests
    {
        [Test]
        public void Test()
        {
            Format.Configure(c =>
                {
                    c.For<Person>()
                        .Write(x => x.Age, o => o.Green())
                        .Write(x => x.Name);
                });

            ClassOutputModel formatter = Format.GetStringFormatter<Person>();

            var model = new Person { Age = 42 };

            IEnumerable<IOutput> outputs = formatter.BuildOutput(model);

            Assert.AreEqual(6, outputs.Count());
            Assert.AreEqual("Age: ", outputs.OfType<PlainString>().First().Output);
        }

    }
}