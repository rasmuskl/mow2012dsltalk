﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using DslTalk.Generator;
using NUnit.Framework;
using System.Linq;

namespace DslTalk.Tests
{
    [TestFixture]
    public class GeneratorTests
    {
        private static readonly Dictionary<string, GeneratedClass> generatedClasses = new Dictionary<string, GeneratedClass>();

        [Test]
        public void GenerateForGeneratorGrammar()
        {
            Type type = typeof(IGeneratorGrammar);

            var builder = new StringBuilder();

            var className = typeof(IGeneratorGrammar).Name.TrimStart('I') + "Lang";
            builder.AppendLine("public class " + className);
            builder.AppendLine("{");

            builder.AppendLine("private readonly " + type.FullName + " _impl;");

            builder.AppendLine("public " + className + "(" + type.FullName + " impl) { _impl = impl; }");

            foreach (var methodInfo in type.GetMethods())
            {
                var methodInfos = type.GetMethods().Except(new[] { methodInfo });

                var returnClassName = GetClassName(methodInfos);

                builder.AppendLine("public " + returnClassName + " " + methodInfo.Name + "()");
                builder.AppendLine("{");

                builder.AppendLine("_impl." + methodInfo.Name + "();");
                builder.AppendLine("return new " + returnClassName + "(_impl);");

                builder.AppendLine("}");
            }

            foreach (var generatedClass in generatedClasses)
            {
                builder.Append(generatedClass.Value);
            }

            builder.AppendLine("}");

            Console.Out.WriteLine(builder.ToString());
        }

        private static string GetClassName(IEnumerable<MethodInfo> methodInfos)
        {
            string classId = typeof(IGeneratorGrammar).Name.TrimStart('I') + methodInfos.Aggregate(string.Empty, (s, m) => s + m.Name);

            GeneratedClass generatedClass;
            if (!generatedClasses.TryGetValue(classId, out generatedClass))
            {
                generatedClass = new GeneratedClass("GeneratedLang", methodInfos);
                generatedClasses.Add(classId, generatedClass);
            }

            return generatedClass.ClassName;
        }

        private class GeneratedClass
        {
            private readonly string _prefix;
            private readonly IEnumerable<MethodInfo> _delegatedMethods;
            private static int _counter = 1;
            private readonly int _number = (_counter++);

            public GeneratedClass(string prefix, IEnumerable<MethodInfo> delegatedMethods)
            {
                _prefix = prefix;
                _delegatedMethods = delegatedMethods;

                ToString();
            }

            public string ClassName
            {
                get { return _prefix + _number; }
            }

            public override string ToString()
            {
                var builder = new StringBuilder();

                builder.AppendLine("public class " + ClassName);
                builder.AppendLine("{");

                builder.AppendLine("private readonly " + typeof(IGeneratorGrammar).FullName + " _impl;");

                builder.AppendLine("public " + ClassName + "(" + typeof(IGeneratorGrammar).FullName + " impl) { _impl = impl; }");

                foreach (var methodInfo in _delegatedMethods)
                {
                    var infos = _delegatedMethods.Except(new[] { methodInfo });

                    if (!_delegatedMethods.Any())
                        continue;

                    var returnClassName = GetClassName(infos);

                    builder.AppendLine("public " + returnClassName + " " + methodInfo.Name + "()");
                    builder.AppendLine("{");

                    builder.AppendLine("_impl." + methodInfo.Name + "();");
                    builder.AppendLine("return new " + returnClassName + "(_impl);");

                    builder.AppendLine("}");
                }

                builder.AppendLine("}");

                return builder.ToString();
            }
        }
    }
}