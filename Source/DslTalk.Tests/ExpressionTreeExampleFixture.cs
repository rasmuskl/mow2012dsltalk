using System.Linq;
using System.Collections.Generic;
using System;
using System.Linq.Expressions;
using NUnit.Framework;

namespace DslTalk.Tests
{
    [TestFixture]
    public class ExpressionTreeExampleFixture
    {
        [Test]
        public void Test()
        {
            GetProperty<Person>(x => x.Friend.Name);
            GetProperty<Person>(x => x.Name);
            GetProperty<Person>(x => x.Age);
        }

        private void GetProperty<T>(Expression<Func<T, object>> propExp)
        {

        }

        private class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public Person Friend { get; set; }
        }
    }
}