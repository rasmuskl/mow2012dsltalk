﻿using NUnit.Framework;

namespace DslTalk.Tests
{
    [TestFixture]
    public class ReflectionHelperTests
    {
        [Test]
        public void Test()
        {
            string propertyName = ReflectionHelper.GetPropertyName<Person>(x => x.Age);

            Assert.AreEqual("Age", propertyName);
        }
    }
}