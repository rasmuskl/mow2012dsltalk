﻿namespace DslTalk.Generator
{
    public interface IGeneratorGrammar
    {
        IGeneratorGrammar Green();
        IGeneratorGrammar AllCaps();
        IGeneratorGrammar Reversed();
    }
}