﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace DslTalk.Generator
{
    public class GeneratorGrammarLang
    {
        private readonly DslTalk.Generator.IGeneratorGrammar _impl;
        public GeneratorGrammarLang(DslTalk.Generator.IGeneratorGrammar impl) { _impl = impl; }

        public GeneratedLang1 Green()
        {
            _impl.Green();
            return new GeneratedLang1(_impl);
        }
        public GeneratedLang5 AllCaps()
        {
            _impl.AllCaps();
            return new GeneratedLang5(_impl);
        }
        public GeneratedLang7 Reversed()
        {
            _impl.Reversed();
            return new GeneratedLang7(_impl);
        }
        public class GeneratedLang3
        {
            private readonly DslTalk.Generator.IGeneratorGrammar _impl;
            public GeneratedLang3(DslTalk.Generator.IGeneratorGrammar impl) { _impl = impl; }
        }
        public class GeneratedLang2
        {
            private readonly DslTalk.Generator.IGeneratorGrammar _impl;
            public GeneratedLang2(DslTalk.Generator.IGeneratorGrammar impl) { _impl = impl; }
            public GeneratedLang3 Reversed()
            {
                _impl.Reversed();
                return new GeneratedLang3(_impl);
            }
        }
        public class GeneratedLang4
        {
            private readonly DslTalk.Generator.IGeneratorGrammar _impl;
            public GeneratedLang4(DslTalk.Generator.IGeneratorGrammar impl) { _impl = impl; }
            public GeneratedLang3 AllCaps()
            {
                _impl.AllCaps();
                return new GeneratedLang3(_impl);
            }
        }
        public class GeneratedLang1
        {
            private readonly DslTalk.Generator.IGeneratorGrammar _impl;
            public GeneratedLang1(DslTalk.Generator.IGeneratorGrammar impl) { _impl = impl; }
            public GeneratedLang2 AllCaps()
            {
                _impl.AllCaps();
                return new GeneratedLang2(_impl);
            }
            public GeneratedLang4 Reversed()
            {
                _impl.Reversed();
                return new GeneratedLang4(_impl);
            }
        }
        public class GeneratedLang6
        {
            private readonly DslTalk.Generator.IGeneratorGrammar _impl;
            public GeneratedLang6(DslTalk.Generator.IGeneratorGrammar impl) { _impl = impl; }
            public GeneratedLang3 Green()
            {
                _impl.Green();
                return new GeneratedLang3(_impl);
            }
        }
        public class GeneratedLang5
        {
            private readonly DslTalk.Generator.IGeneratorGrammar _impl;
            public GeneratedLang5(DslTalk.Generator.IGeneratorGrammar impl) { _impl = impl; }
            public GeneratedLang2 Green()
            {
                _impl.Green();
                return new GeneratedLang2(_impl);
            }
            public GeneratedLang6 Reversed()
            {
                _impl.Reversed();
                return new GeneratedLang6(_impl);
            }
        }
        public class GeneratedLang7
        {
            private readonly DslTalk.Generator.IGeneratorGrammar _impl;
            public GeneratedLang7(DslTalk.Generator.IGeneratorGrammar impl) { _impl = impl; }
            public GeneratedLang4 Green()
            {
                _impl.Green();
                return new GeneratedLang4(_impl);
            }
            public GeneratedLang6 AllCaps()
            {
                _impl.AllCaps();
                return new GeneratedLang6(_impl);
            }
        }
    }
}