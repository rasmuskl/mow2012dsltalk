﻿namespace DslTalk
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string TwitterHandle { get; set; }
        public string Email { get; set; }
    }
}