using System;
using System.Collections.Generic;
using System.Linq;

namespace DslTalk.Output
{
    public class ConsoleOutputRenderer
    {
        public void Render(IEnumerable<IOutput> outputs)
        {
            foreach (var output in outputs.Where(x => x != null))
            {
                var plainString = output as PlainString;
                var coloredString = output as ColoredString;

                if(plainString != null)
                {
                    Console.Write(plainString.Output);
                } 
                else if(coloredString != null)
                {
                    ConsoleColor color = Console.ForegroundColor;
                    Console.ForegroundColor = coloredString.Color;
                    Console.Write(coloredString.Output);
                    Console.ForegroundColor = color;
                }
                else
                {
                    throw new Exception("Unknown output. Implement a visitor :-)");
                }
            }
        }
    }
}