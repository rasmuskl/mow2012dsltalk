﻿namespace DslTalk.Output
{
    public class PlainString : IOutput
    {
        public string Output { get; private set; }

        public PlainString(string output)
        {
            Output = output;
        }
    }
}