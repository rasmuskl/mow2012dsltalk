﻿using System;

namespace DslTalk.Output
{
    public class ColoredString : IOutput
    {
        public string Output { get; set; }
        public ConsoleColor Color { get; set; }

        public ColoredString(string output, ConsoleColor color)
        {
            Output = output;
            Color = color;
        }
    }
}