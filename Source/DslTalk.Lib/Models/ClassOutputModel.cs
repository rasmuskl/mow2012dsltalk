using System;
using System.Collections.Generic;
using System.Linq;
using DslTalk.Output;

namespace DslTalk.Models
{
    public class ClassOutputModel
    {
        private readonly Type _type;
        private readonly List<PropertyOutputModel> _properties = new List<PropertyOutputModel>();

        public ClassOutputModel(Type type)
        {
            _type = type;
        }

        public IEnumerable<IOutput> BuildOutput(object o)
        {
            
            return _properties.SelectMany(propertyModel => propertyModel.BuildOutput(o));
        }

        public PropertyOutputModel GetPropertyModel(string propertyName)
        {
            PropertyOutputModel propertyModel = _properties.FirstOrDefault(x => x.PropertyName == propertyName);
            
            if(propertyModel == null)
            {
                propertyModel = new PropertyOutputModel(_type, propertyName);
                _properties.Add(propertyModel);
            }

            return propertyModel;
        }
    }
}