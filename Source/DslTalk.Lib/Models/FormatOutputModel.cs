using System;
using System.Collections.Generic;

namespace DslTalk.Models
{
    public class FormatOutputModel
    {
        private readonly Dictionary<Type, ClassOutputModel> _classOutputModels = new Dictionary<Type, ClassOutputModel>();

        public ClassOutputModel GetClassModel<T>()
        {
            ClassOutputModel outputModel;

            if (_classOutputModels.TryGetValue(typeof(T), out outputModel))
            {
                return outputModel;
            }

            outputModel = new ClassOutputModel(typeof(T));
            _classOutputModels[typeof(T)] = outputModel;

            return outputModel;
        }

    }
}