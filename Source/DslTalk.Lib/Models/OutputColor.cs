using System.Linq;
using System.Collections.Generic;
using System;

namespace DslTalk.Models
{
    public enum OutputColor
    {
        Standard,
        Green,
        Rainbow,
        Red
    }
}