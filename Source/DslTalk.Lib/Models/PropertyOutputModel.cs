using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using DslTalk.Output;
using System.Linq;

namespace DslTalk.Models
{
    public class PropertyOutputModel
    {
        private readonly List<Predicate<object>> _omitPredicates = new List<Predicate<object>>();
        private readonly Type _parentType;
        public string PropertyName { get; set; }
        public OutputColor Color { get; set; }

        public bool Reversed { get; set; }
        public bool AllCaps { get; set; }

        private static readonly ConsoleColor[] RainbowColors = new[] { ConsoleColor.Red, ConsoleColor.Yellow, ConsoleColor.Green, ConsoleColor.Blue, ConsoleColor.Magenta };

        public PropertyOutputModel(Type parentType, string propertyName)
        {
            _parentType = parentType;
            PropertyName = propertyName;
        }

        public IEnumerable<IOutput> BuildOutput(object o)
        {
            PropertyInfo propertyInfo = _parentType.GetProperty(PropertyName);

            string name = propertyInfo.Name;
            object value = propertyInfo.GetValue(o, null);

            string str = value == null ? string.Empty : value.ToString();

            if (_omitPredicates.Any(x => x(value)))
            {
                yield break;
            }

            yield return new PlainString(string.Format("{0}: ", name));

            if (Reversed)
            {
                str = new string(str.Reverse().ToArray());
            }

            if (AllCaps)
            {
                str = str.ToUpperInvariant();
            }

            if (Color == OutputColor.Green)
            {
                yield return new ColoredString(str, ConsoleColor.Green);
            }
            else if (Color == OutputColor.Red)
            {
                yield return new ColoredString(str, ConsoleColor.Red);
            }
            else if (Color == OutputColor.Rainbow)
            {
                for (var i = 0; i < str.Length; i++)
                {
                    ConsoleColor color = RainbowColors[i % RainbowColors.Length];
                    yield return new ColoredString(str[i].ToString(CultureInfo.InvariantCulture), color);
                }
            }
            else
            {
                yield return new PlainString(str);
            }

            yield return new PlainString(Environment.NewLine);
        }

        public void AddOmitPredicate(Predicate<object> predicate)
        {
            _omitPredicates.Add(predicate);
        }
    }
}
