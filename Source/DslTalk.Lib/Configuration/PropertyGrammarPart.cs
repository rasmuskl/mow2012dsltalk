using System;
using DslTalk.Models;

namespace DslTalk.Configuration
{
    public class PropertyGrammarPart<T>
    {
        private readonly PropertyOutputModel _propertyModel;

        public PropertyGrammarPart(PropertyOutputModel propertyModel)
        {
            _propertyModel = propertyModel;
        }

        public PropertyGrammarPart<T> OmitIf(Predicate<T> omitPredicate)
        {
            _propertyModel.AddOmitPredicate(x => omitPredicate((T)x));
            return this;
        }

        public PropertyGrammarPart<T> Green()
        {
            _propertyModel.Color = OutputColor.Green;
            return this;
        }

        public PropertyGrammarPart<T> Red()
        {
            _propertyModel.Color = OutputColor.Red;
            return this;
        }

        public PropertyGrammarPart<T> Rainbow()
        {
            _propertyModel.Color = OutputColor.Rainbow;
            return this;
        }

        public PropertyGrammarPart<T> Reversed()
        {
            _propertyModel.Reversed = true;
            return this;
        }

        public PropertyGrammarPart<T> AllCaps()
        {
            _propertyModel.AllCaps = true;
            return this;
        }
    }
}