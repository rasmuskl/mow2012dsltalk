using System;
using System.Linq.Expressions;
using DslTalk.Models;

namespace DslTalk.Configuration
{
    public class ClassGrammarPart<T>
    {
        private readonly ClassOutputModel _classModel;

        public ClassGrammarPart(ClassOutputModel classModel)
        {
            _classModel = classModel;
        }

        public ClassGrammarPart<T> Write<TP>(Expression<Func<T, TP>> property, Func<PropertyGrammarPart<TP>, object> options)
        {
            string propertyName = ReflectionHelper.GetPropertyName(property);
            var propertyModel = _classModel.GetPropertyModel(propertyName);

            var propertyGrammar = new PropertyGrammarPart<TP>(propertyModel);
            options(propertyGrammar);

            return this;
        }

        public ClassGrammarPart<T> Write(Expression<Func<T, object>> property)
        {
            _classModel.GetPropertyModel(ReflectionHelper.GetPropertyName(property));
            return this;
        }
    }
}