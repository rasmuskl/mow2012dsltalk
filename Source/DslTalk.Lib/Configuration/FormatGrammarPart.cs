using DslTalk.Models;

namespace DslTalk.Configuration
{
    public class FormatGrammarPart
    {
        private readonly FormatOutputModel _formatOutputModel = new FormatOutputModel();

        public ClassGrammarPart<T> For<T>()
        {
            ClassOutputModel formatter = _formatOutputModel.GetClassModel<T>();
            return new ClassGrammarPart<T>(formatter);
        }

        public FormatOutputModel GetModel()
        {
            return _formatOutputModel;
        }
    }
}