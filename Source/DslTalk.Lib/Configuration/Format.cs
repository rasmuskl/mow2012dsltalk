﻿using System;
using System.Collections.Generic;
using DslTalk.Models;

namespace DslTalk.Configuration
{
    public class Format
    {
        private static FormatOutputModel _formatOutputModel;

        public static void Configure(Action<FormatGrammarPart> configurationAction)
        {
            var grammar = new FormatGrammarPart();
            configurationAction(grammar);
            _formatOutputModel = grammar.GetModel();
        }

        public static ClassOutputModel GetStringFormatter<T>()
        {
            return _formatOutputModel.GetClassModel<T>();
        }

        public static void AddConvention(Action<ConventionPart> func)
        {

        }
        
        public static void AddRegistry<T>()
        {
            
        }

        public static void ScanAssembliesForRegistries()
        {
            
        }
    }
}