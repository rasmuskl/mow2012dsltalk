﻿using System;
using System.Linq.Expressions;

namespace DslTalk
{
    public static class ReflectionHelper
    {
        public static string GetPropertyName<T, TP>(Expression<Func<T, TP>> propertyFunc)
        {
            return GetPropertyNameInternal(propertyFunc.Body);
        }

        public static string GetPropertyName<T>(Expression<Func<T, object>> propertyFunc)
        {
            return GetPropertyNameInternal(propertyFunc.Body);
        }

        private static string GetPropertyNameInternal(Expression body)
        {
            // Unwrap boxing
            if (body is UnaryExpression)
            {
                body = ((UnaryExpression) body).Operand;
            }

            var memberExpression = body as MemberExpression;

            if (memberExpression == null)
            {
                throw new Exception("Expression wasn't a valid MemberExpression.");
            }

            return memberExpression.Member.Name;
        }
    }
}